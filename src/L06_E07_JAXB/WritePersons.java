package L06_E07_JAXB;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class WritePersons {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.setFirstname("Kirk");
		p1.setLastname("Douglas");
		p1.setBirthday(9);
		p1.setBirthmonth(12);
		p1.setBirthyear(1916);
		
		Person p2 = new Person();
		p2.setFirstname("Diana");
		p2.setLastname("Douglas");
		p2.setBirthday(22);
		p2.setBirthmonth(1);
		p2.setBirthyear(1923);
		
		Person p3 = new Person();
		p3.setFirstname("Michael");
		p3.setMiddlename("Kirk");
		p3.setLastname("Douglas");
		p3.setBirthday(25);
		p3.setBirthmonth(9);
		p3.setBirthyear(1944);
		p3.setFather(p1);
		p3.setMother(p2);
		
		Persons persons = new Persons();
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);
		
		System.out.println(p1+"");
		System.out.println(p2+"");
		System.out.println(p3+"");
		
		Persons.writePersons(persons,"test.xml");
		
		

	}

}
