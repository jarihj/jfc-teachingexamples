package L06_E07_JAXB;

import java.util.List;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Persons {
	@XmlElement (name="person")
	private List<Person> persons;

	public Persons() {
		setPersons(new ArrayList<Person>());
	}
	
	public static Persons readPersons(String filename) {
		Persons p=null;
		try {
			JAXBContext context = JAXBContext.newInstance(Persons.class, Person.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			p = (Persons) unmarshaller.unmarshal(new File(filename));
			System.out.println(p);
		}
		catch (JAXBException e) {System.err.println("JAXBE "+e);};
		return p;
	}

	public static void writePersons(Persons p, String filename) {
		writePersons(p,filename,false);
	}

	public static void writePersons(Persons p, String filename, boolean printoutonly) {
		try {
			JAXBContext context = JAXBContext.newInstance(Persons.class, Person.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			if (!printoutonly) marshaller.marshal(p,new File(filename));
			marshaller.marshal(p,System.out);
		}
		catch (JAXBException e) {System.err.println("JAXBE "+e);};
		
	}
	

	

	
	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	
	public void add(Person p) {
		persons.add(p);
	}
	
	public int getSize() {
		return persons.size();
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		Iterator<Person> it = persons.iterator();
		while (it.hasNext()) {
			s.append(it.next()+"\n");
		}
		return s.toString();
	}

}
