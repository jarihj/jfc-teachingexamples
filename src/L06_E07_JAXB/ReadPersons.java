package L06_E07_JAXB;

public class ReadPersons {

	public static void main(String[] args) {
		Persons persons=Persons.readPersons("test.xml");
		System.out.printf("Size of list after load: %d\n", persons.getSize());
		
		
		Persons.writePersons(persons,"test.xml",true);
	}

}

