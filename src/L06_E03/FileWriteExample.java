package L06_E03;
import java.io.*;

public class FileWriteExample {
	public static void main(String args[]) {
		try {
			FileWriter fw = new FileWriter("filenameL6_E3.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			pw.println("Programming in ");
			pw.println("Java");
			pw.close();
		}
		catch (IOException ioerr) {
			System.err.println("error"+ioerr);
		}
	}

}
