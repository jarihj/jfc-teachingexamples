package L06_E04;
import java.io.*;

public class FileReadExample_b {
	public static void main(String args[]) throws IOException {
		try {
			FileReader fr = new FileReader("filenameL6_E3a.txt");
			BufferedReader fileIn = new BufferedReader(fr);
			String str = fileIn.readLine();
			while (str!=null) {
				System.out.println(" > "+str);
				str = fileIn.readLine();
			}
			fileIn.close();
		}
		catch (FileNotFoundException ioerr) {
			System.err.println("File not found: "+ioerr);
		}
	}

}
