package L06_E04;
import java.io.*;

public class FileReadExample {
	public static void main(String args[]) {
		try {
			FileReader fr = new FileReader("filenameL6_E3a.txt");
			BufferedReader fileIn = new BufferedReader(fr);
			String str = fileIn.readLine();
			while (str!=null) {
				System.out.println(" > "+str);
				str = fileIn.readLine();
			}
			fileIn.close();
		}
		catch (IOException ioerr) {
			System.err.println("error"+ioerr);
		}
	}

}
