 package L07_E05_ExploreEvents;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class EventExplorer extends JFrame implements ActionListener {
	
	JButton btnA, btnB, btnC;
	JTextArea txt;
	JPanel pnl;
	
	class BtnAEvent implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			showAEInfo(ae);
		}
	}
	
	public EventExplorer() {
		setLayout(new BorderLayout());
		pnl = new JPanel();
		pnl.setLayout(new FlowLayout());
		add(pnl,BorderLayout.PAGE_START);
		
		btnA = new JButton("I am A");
		pnl.add(btnA);
		btnA.addActionListener(new BtnAEvent());
		
		btnB = new JButton("I am B");
		pnl.add(btnB);
		btnB.addActionListener(this);

		btnC = new JButton("I am C");
		pnl.add(btnC);
		btnC.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent ae) {
										showAEInfo(ae);
									}
							});

		
		txt = new JTextArea();
		txt.setFont(new Font("Courier", Font.PLAIN,12));
		add(txt,BorderLayout.CENTER);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(600, 500); // Window Size
		setVisible(true); // Show the window
		
	}
		
	public void showAEInfo(ActionEvent ae) {
		txt.setText("");
		String fs = "%-20s:: %s\n";
		txt.append(String.format(fs,"getActionCommand",ae.getActionCommand()));
		txt.append(String.format(fs,"getModifiers",ae.getModifiers()+""));
		txt.append(String.format(fs,"getWhen",ae.getWhen()+""));
		txt.append(String.format(fs,"paramString",ae.paramString()+""));
		txt.append(String.format(fs,"getID",ae.getID()+""));
		txt.append(String.format(fs,"toString",ae.toString()+""));
		txt.append(String.format(fs,"getSource",ae.getSource()+""));
		
		
	}

	public void actionPerformed(ActionEvent ae) {
		showAEInfo(ae);
	}
	
	
	public static void main(String[] args) {
		EventExplorer sw = new EventExplorer();
	}

}
