package L11_E08_MethodReferences;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MethodTest2_SortTest3 {
	public static void main(String[] args) {
		List<String> names = Arrays.asList("C++", "Java", "JSP", "Python","Pascal",	"C#", "Cobol");
		Collections.sort(names, (a,b) -> a.compareTo(b));
		System.out.println(names.toString());
	}

}
