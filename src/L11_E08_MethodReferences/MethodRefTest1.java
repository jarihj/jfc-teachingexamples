package L11_E08_MethodReferences;

import java.util.function.Function;

public class MethodRefTest1 {
	public static void main(String[] args) {
		Function<String, Integer> test1 = String::length;
		Function<String, Integer> test2 = s -> s.length();
		String s = "Lambdas are fun!";
		System.out.println(test1.apply(s)); // 15
		System.out.println(test2.apply(s)); // 15
	}

}
