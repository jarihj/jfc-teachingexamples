package L03_E2_Interfaces;

interface IEatable {
	boolean isAnimal();
	boolean hasTail();
	float getWeight();
}

interface IHuntableAndEatable extends IEatable {
	boolean canFly();
	float getEscapeSpeed();
}

interface IDrawable {
	void drawMe(int x, int y);
}
