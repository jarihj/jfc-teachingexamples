package L03_E2_Interfaces;

public class SpaceAlien extends Feline implements IDrawable{
	public void drawMe(int x, int y) {
		System.out.println("But I am a space-alien");
	}
	
	public void speak() { System.out.println("uhuh"); }
	
	boolean canEat(IEatable aFood) { return true; } //Eats anything

	boolean canCatch(IHuntableAndEatable aVictim) { return true; }
}