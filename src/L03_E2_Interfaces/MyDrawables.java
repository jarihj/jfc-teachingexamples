package L03_E2_Interfaces;

public class MyDrawables {
	
	private IDrawable[] drawables = new IDrawable[3];
	
	public MyDrawables() // Create and "draw" the IDrawables 
	{
		drawables[0]=new HouseCat();
		drawables[1]=new SpaceAlien();
		drawables[2]=new Rat();
		for (int i=0;i<drawables.length;i++)
		drawables[i].drawMe(i*5,i*5);
	}

	public IDrawable getDrawable(int idx) {
		return drawables[idx];
	}

	public static void main(String[] args){
		MyDrawables theDrawables = new MyDrawables();
		IHuntableAndEatable food = new Rat(); // A cat eats a rat?
		Feline cat = new HouseCat();
		if(cat.canEat(food))
		System.out.println("A cat can eat a " + food);
		else
		System.out.println("No, a cat cannot eat a " + food);
		// Well then, can a cat catch a rat??
		if(cat.canCatch(food))
		System.out.println("A cat can catch a " + food);
		else
		System.out.println("No, a cat cannot catch a " + food);
	}
}