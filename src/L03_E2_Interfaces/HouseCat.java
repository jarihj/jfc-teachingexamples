package L03_E2_Interfaces;

public class HouseCat extends Feline implements IDrawable {
	public void drawMe(int x, int y) {
		System.out.println("I am a housecat");
	}
	public void speak() {
		System.out.println("mjau");
	}
	boolean canEat(IEatable aFood) { // Good enough?
		if(aFood.isAnimal() && aFood.hasTail() && aFood.getWeight() < 0.25) // A rat maybe...?
			return true;
		return false;
	}
	
	boolean canCatch(IHuntableAndEatable aVictim) {
		if(!aVictim.canFly() && aVictim.getEscapeSpeed() < 12.0) // Testing if the Housecat can catch the prey.
			return true; // Not a bird.
		return false;
	}
}