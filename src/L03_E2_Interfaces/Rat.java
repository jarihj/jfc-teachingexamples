package L03_E2_Interfaces;

public class Rat implements IHuntableAndEatable, IDrawable {
	// IHuntableAndEatable
	public boolean isAnimal() { return true; }
	
	public boolean hasTail() { return true; }
	
	public float getWeight() { return 0.025f; }
	
	public boolean canFly() { return false;}
	
	public float getEscapeSpeed() { return 10.0f; }
	
	// IDrawable
	public void drawMe(int x, int y) {
		System.out.println("I am a rat."); 
	}
	
	public String toString() { return "rat"; }

}
