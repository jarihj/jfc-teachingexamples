package L03_E2_Interfaces;
/**
 * This is an abstract class acting as an ancestor for all feline animals.
 * @author JariH
 *
 */
abstract class Feline // Family of cat species
{ 
	/**
	 * Abstract method speak, for ......
	 */
	abstract void speak();
	abstract boolean canEat(IEatable aFood);
	abstract boolean canCatch(IHuntableAndEatable aVictim);
}

