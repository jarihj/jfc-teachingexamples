package L09_E04_TestJoin;

public class TestingJoin {
	
	public static void main(String[] args) {
		System.out.println("Start");
		Animal cow = new Animal("Cow","muuu");
		Animal cat = new Animal("Cat","mjav");
		cow.talk();
		cat.talk();
		try {
			cow.join();
			cat.join();
		}
		catch (InterruptedException ie) {}
		System.out.println("End");
	}
}
