package L09_E04_TestJoin;

public class Animal extends Thread {
	private String type, sound;
	public Animal(String type, String sound) {
		this.type = type; 
		this.sound = sound;
	}
	public void talk() {
		start();
	}
	public void zz() {
		int ms = (int)(Math.random() * 1000);
		try {
			sleep(ms * 4); // sleeps max 4 sek
		}
		catch (InterruptedException ie) {
			System.out.println("Animal has awaken");
		}
	}
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println(sound + " " + (i+1) + " from " + type);
			zz();
		}
	}
}