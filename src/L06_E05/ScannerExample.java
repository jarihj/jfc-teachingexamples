package L06_E05;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;

public class ScannerExample {
	
	public static void main(String args[]) {
		String str, country; int g, s, b;
		File myFile = new File("data.txt");
		try {
			Scanner inFile = new Scanner(myFile);
			while (inFile.hasNextLine()) {
				str		= inFile.nextLine();
				Scanner row = new Scanner(str);
				row.useDelimiter(";");
				country = row.next();
				g       = row.nextInt();
				s       = row.nextInt();
				b       = row.nextInt();
				System.out.printf("%s has %d gold, %d silver and %d bronze medals.\n"
						,country,g,s,b);
				row.close();
			} //while
			inFile.close();
		} // try
		catch (FileNotFoundException e) {System.err.println("FNFE Error:: "+e); }
		catch (InputMismatchException e) {System.err.println("IME Error:: "+e); }
	}
}
