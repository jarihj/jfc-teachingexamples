 package L07_E04_EventAdapter;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;

import javax.swing.*;

public class SimpleWindowA extends JFrame  {
	
	class MouseMoveEvent extends MouseMotionAdapter {
		public void mouseMoved(MouseEvent me) { 
			lbl.setText(String.format("(x,y)=(%d,%d)",me.getX(),me.getY()));
		}
	}
	
	class ButtonEvent implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			lbl.setText("Button pressed");
		}
	}
	
	JLabel lbl;
	JButton btn;
	public SimpleWindowA() {
		setLayout(new FlowLayout());
		lbl = new JLabel("...");
		add(lbl);
		btn = new JButton("Press me");
		add(btn);
		btn.addActionListener(new ButtonEvent());

		this.addMouseMotionListener(new MouseMoveEvent());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}
	
	public static void main(String[] args) {
		SimpleWindowA sw = new SimpleWindowA();
	}

}
