package L11_E05_StdInt_Predicate;

import java.util.function.Predicate;

public class PredicateTest1 {
	
	public static void main(String[] args) {
		Predicate <Integer> greaterThanTen = i -> i>10;
		System.out.println(greaterThanTen.test(14));
		System.out.println(greaterThanTen.test(7));
		boolean b = greaterThanTen.test(14);
		System.out.println("test:"+b);
	}

}
