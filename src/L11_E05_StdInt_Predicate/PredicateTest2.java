package L11_E05_StdInt_Predicate;

import java.util.function.Predicate;

public class PredicateTest2 {
	public static void process(int number, Predicate<Integer> predicate) {
		if (predicate.test(number)) {
			System.out.printf("Number %d passed the test.\n",number);
		}
	}
		
	public static void main(String[] args) {
		Predicate <Integer> greaterThanTen = i -> i>10;
		process(11, i -> i>7);
		process(12,greaterThanTen);
		
	}

}
