package L11_E05_StdInt_Predicate;

import java.util.function.Predicate;

public class PredicateTest3 {
	public static void process(int number, Predicate<Integer> predicate) {
		if (predicate.test(number)) {
			System.out.printf("Number %d passed the test.\n",number);
		}
	}
		
	public static void main(String[] args) {
		Predicate <Integer> greaterThanTen = i -> i>10;
		Predicate <Integer> lessThanTwenty = i -> i<20;
		greaterThanTen.and(lessThanTwenty).test(11);
		System.out.println("12:: Not greater than 10: "+greaterThanTen.negate().test(12));
		System.out.println("13:: Greater than 10 and greater than 14: "
				+greaterThanTen.and(i->i>14).test(13));
		System.out.println("14:: Greater than 10 and not greater than 16: "
				+greaterThanTen.and(i->i>16).negate().test(14));
		System.out.println("14:: Not greater than 10 and not greater than 16: "
				+greaterThanTen.negate().and(i->i>16).negate().test(14));
	}

}
