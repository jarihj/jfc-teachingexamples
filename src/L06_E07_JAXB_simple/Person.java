package L06_E07_JAXB_simple;

public class Person {
	private String firstname="";
	private String middlename="";
	private String lastname="";
	private int birthyear=0;
	private int birthmonth=0;
	private int birthday=0;
	private double height=0;
	private double weight=0;
	private Person father;
	private Person mother;
	
	public Person() {
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getBirthyear() {
		return birthyear;
	}

	public void setBirthyear(int birthyear) {
		if (birthyear<1900) birthyear+=1900;
		this.birthyear = birthyear;
	}

	public int getBirthmonth() {
		return birthmonth;
	}

	public void setBirthmonth(int birthmonth) {
		this.birthmonth = birthmonth;
	}

	public int getBirthday() {
		return birthday;
	}

	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Person getFather() {
		return father;
	}

	public void setFather(Person father) {
		this.father = father;
	}

	public Person getMother() {
		return mother;
	}

	public void setMother(Person mother) {
		this.mother = mother;
	}
	
	public String getName() {
		String res="";
		if (firstname.length()>0) res+=firstname+" ";
		if (middlename.length()>0) res+=middlename+" ";
		if (lastname.length()>0) res+=lastname+" ";
		res=res.trim();
		return res;
	}
	
	public String getBirth() {
		if (birthday*birthmonth*birthyear>0)  return String.format("%02d-%02d-%4d",birthday,birthmonth,birthyear);
		else return"";
	}
	
	public String toString() {
		String a="", b="",c="",d="";
		a = getName();
		b = getBirth();
		if (father!=null) c=father.getName();
		if (mother!=null) d=mother.getName();
		if ((c.length()>0)&&(d.length()>0)) c=String.format(" (%s;%s)",c,d);
		else if (c.length()>0) c=String.format(" (%s)",c); 
		else if (d.length()>0) c=String.format(" (%s)",d);
		else c="";
		
		if (b.length()>0) return String.format("%s, %s%s",a,b,c);
		else return String.format("%s%s",a,c);
	}

}
