package L06_E07_JAXB;

import java.util.*;
// Simplyfied version of Persons
public class Persons {
	private List<Person> persons;

	public Persons() {
		setPersons(new ArrayList<Person>());
	}
	
	public void add(Person p) {
		persons.add(p);
	}
	
	public int getSize() {
		return persons.size();
	}

}
