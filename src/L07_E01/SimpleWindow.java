package L07_E01;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.*;

public class SimpleWindow extends JFrame {
	public SimpleWindow() {
		JButton btn=new JButton("I am a JButton");
		btn.setSize(new Dimension(50,20));
		add(btn);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
		
		JOptionPane.showMessageDialog(null,getLayout().toString());
	}

	public static void main(String[] args) {
		SimpleWindow sw = new SimpleWindow();
	}

}
