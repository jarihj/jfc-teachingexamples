package L09_E03_ThreadInterrupt;

public class ThreadInterruptDemo {

	public static void main(String[] args) {
		MyThread mt = new MyThread();
		mt.start();
		(new InterruptorThread(3,mt)).start();
	}

}
