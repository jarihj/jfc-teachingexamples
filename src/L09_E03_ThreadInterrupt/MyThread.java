package L09_E03_ThreadInterrupt;

public class MyThread extends Thread {
	boolean bInterrupted = false;
	int iStart;
	public MyThread() {
		this(0);
	}
	
	public MyThread(int iStart) {
		this.iStart=iStart;
	}
	
	public void interrupt() {bInterrupted=true;}
	
	public void run()  {
		System.out.println("Starting thread");
		int i = iStart;
		while (!bInterrupted) {
			System.out.printf("The number is: %d. The square is %d and the squareroot is %f\n",
								i,i*i,Math.sqrt(i)); 
			try {sleep((int) (Math.random()*1000));} 
			catch (InterruptedException ie) {System.err.println("Thread interrupted.");}
			i++;
		}
		System.out.println("Thread ending");
	}
}