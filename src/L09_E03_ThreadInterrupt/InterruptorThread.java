package L09_E03_ThreadInterrupt;

public class InterruptorThread extends Thread {
	MyThread mt;
	int iWait;
	public InterruptorThread() {
		this(1,null);
	}
	
	public InterruptorThread(int iWait, MyThread mt) {
		this.iWait = iWait;
		this.mt=mt;
	}
	public void run()  {
		System.out.println("Starting InterruptorThread");
		for (int i=0; i<iWait; i++) { 
			System.out.printf("Interruptor, waiting: %d\n",i); 
			try {sleep (1000);} catch (InterruptedException ie) {}
		}
		if (mt!=null) {mt.interrupt();System.out.println("InterruptorThread: Ending other thread.");} 
		System.out.println("InterruptorThread ending.");
	}
}