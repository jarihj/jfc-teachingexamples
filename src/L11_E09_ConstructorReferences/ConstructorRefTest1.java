package L11_E09_ConstructorReferences;

import java.util.function.Supplier;

public class ConstructorRefTest1 {

	public static void main(String[] args) {
		Supplier<User> f1 = User::new;
		User u1 = f1.get();
		u1.setName("Jari");
		System.out.println(u1.getName());

	}

}
