package L11_E06_StdInt_Function;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class FunctionTest3 {
	static List<User> users;
	public static void init() {
		users = new ArrayList<>();
		users.add(new User("Jari", "admin"));
		users.add(new User("Hilmar", "member"));
		users.add(new User("Hannes", "member"));
		users.add(new User("Qin", "admin"));
	}
	
	public static List<String> getUsersAsString(List<User> users,Function<User, String> function) {
		List<String> result = new ArrayList<>();
		for (User user : users) {
			String s = function.apply(user);
			result.add(s);
		}
		return result;
	}
	
	public static void main(String[] args) {
		init();
		List<String> userStrings = getUsersAsString(users,user -> user.getName() + "|" + user.getRole());
		System.out.println(userStrings.toString());
		
	}

}
