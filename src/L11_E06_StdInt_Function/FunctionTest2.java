package L11_E06_StdInt_Function;

import java.util.function.Function;

public class FunctionTest2 {
	
	public static void main(String[] args) {
		Function<String, Integer> stringLength = s -> s.length();
		Function<Integer, Double> half = i -> i / 2d;
		System.out.println(stringLength.andThen(half).apply("Lambdas are fun!!")); 
	}

}
