package L11_E06_StdInt_Function;

import java.util.function.Function;

public class FunctionTest1 {
	
	public static void main(String[] args) {
		Function<String,Integer> stringLength = s -> s.length();
		System.out.println(stringLength.apply("Lambdas are fun!"));
	}

}
