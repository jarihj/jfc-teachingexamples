package L07_E02_LayoutManagers;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.*;

public class SimpleManyButtons_Flow extends JFrame {
	JButton btn;
	public SimpleManyButtons_Flow() {
		setLayout(new FlowLayout());
		String str="I am a JButton";
		for (int i=0;i<10;i++) {
			btn=new JButton(str);
			btn.setSize(new Dimension(50,20));
			add(btn);
			str+=" too";
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}

	public static void main(String[] args) {
		SimpleManyButtons_Flow sw = new SimpleManyButtons_Flow();
	}

}
