 package L07_E02_LayoutManagers;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;

public class SimpleWindow_Flow extends JFrame  {
	public SimpleWindow_Flow() {
		setLayout(new FlowLayout());
		JButton btn=new JButton("I am a JButton");
		btn.setSize(new Dimension(50,20));
		add(btn);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}
	


	public static void main(String[] args) {
		SimpleWindow_Flow sw = new SimpleWindow_Flow();
	}
	
	

}
