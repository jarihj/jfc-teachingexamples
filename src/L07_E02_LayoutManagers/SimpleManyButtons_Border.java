package L07_E02_LayoutManagers;

import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class SimpleManyButtons_Border extends JFrame {
	JButton btn;
	public SimpleManyButtons_Border() {
		setLayout(new BorderLayout());
		String str="I am a JButton";

		btn=new JButton(str);
		btn.setSize(new Dimension(50,20));
		add(btn,BorderLayout.NORTH);
		str+=" too";
		
		btn=new JButton(str);
		btn.setSize(new Dimension(50,20));
		add(btn,BorderLayout.WEST);
		str+=" too";

		
		btn=new JButton(str);
		btn.setSize(new Dimension(50,20));
		add(btn,BorderLayout.EAST);
		str+=" too";

		
		btn=new JButton(str);
		btn.setSize(new Dimension(50,20));
		add(btn,BorderLayout.SOUTH);
		str+=" too";

		
		btn=new JButton(str);
		btn.setSize(new Dimension(50,20));
		add(btn,BorderLayout.CENTER);
		str+=" too";

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}

	public static void main(String[] args) {
		SimpleManyButtons_Border sw = new SimpleManyButtons_Border();
	}

}
