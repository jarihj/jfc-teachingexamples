package L07_E02_LayoutManagers;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class SimpleManyButtons_Grid extends JFrame {
	JButton btn;
	public SimpleManyButtons_Grid() {
		setLayout(new GridLayout(4,3));
		String str="I am a JButton";
		for (int i=0;i<10;i++) {
			btn=new JButton(str);
			btn.setSize(new Dimension(50,20));
			add(btn);
			str+=" too";
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}

	public static void main(String[] args) {
		SimpleManyButtons_Grid sw = new SimpleManyButtons_Grid();
	}

}
