package L05_E2;
import java.util.ArrayList;
import java.util.ListIterator;
 
public class ListIteratorExample {
 
    public static void main(String[] args) {
 
        ArrayList < String > celebrities = new ArrayList < String > ();
 
        celebrities.add("Tom Cruise");
        celebrities.add("Will Smith");
        celebrities.add("Jackie Chan");
        celebrities.add("Akshay Kumar");
 
        ListIterator < String > listIterator = celebrities.listIterator();
 
        System.out.println("Forword direction:- ");
 
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
 
        System.out.println("Backward direction:- ");
 
        while (listIterator.hasPrevious()) {
            System.out.println(listIterator.previous());
        }
 
        if (listIterator.hasPrevious())
        	listIterator.previous();
        else {System.out.println("No previous element.");}
    }
}