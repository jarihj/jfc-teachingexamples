package L10_E03_DatagramComm;

/**
 * @author Robert Jonsson (robert.jonsson@miun.se), ITM Ístersund
 * @author JariH (jarih@setur.fo).
 * @version 1.1
 * @file L10_E03_DatagramComm.ReceiveDatagram
 */

import java.net.*;
import java.io.*;

public class ReceiveDatagram
{
	public static void main(String[] args)
	{
		try
		{
			byte[] data = new byte[1024];
			DatagramPacket packet = new DatagramPacket(data,data.length);
			DatagramSocket socket = new DatagramSocket(1234);
			System.out.println("Waiting for packet...");
			socket.receive(packet);
			socket.close();
			InetAddress origin = packet.getAddress();
			String message = new String(packet.getData(),0,packet.getLength());
			System.out.printf("Message from '%s' ::\n",origin.getHostAddress());
			System.out.println(message);
			System.out.println(":::");
		}
		catch (SocketException socketErr)
		{
			// Could not create DatagramSocket
			socketErr.printStackTrace();
		}
		catch (IOException ioErr)
		{
			// Could not receive packet
			ioErr.printStackTrace();
		}
    }
}