package L10_E03_DatagramComm;

/**
 * @author Robert Jonsson (robert.jonsson@miun.se), ITM Ístersund
 * @author JariH (jarih@setur.fo).
 * @version 1.1
 * @file L10_E03_DatagramComm.SendDatagram
 */

import java.net.*;
import java.io.*;

public class SendDatagram
{
	public static void main(String[] args)
	{
		try
		{
			// Prepare the message for sending
			String message= "The number pi is very central in math. It is approximately : pi = 3.3.141592653589793238462643383279502884197169399375105820974944592307816406286208998";
			byte[] data = message.getBytes();

			// Create InetAddress for the host to which the message is to be sent.
			// Use getLocalHost as we are send to ourselves
			InetAddress dest = InetAddress.getLocalHost();

			// Create datagram and specify that the receiving host is listening on port 1234.
			DatagramPacket packet = new DatagramPacket(data, data.length, dest, 1234);

			// Create a socket for sending. Close when we are done.
			DatagramSocket socket = new DatagramSocket();

			System.out.print("Sending message... ");
			socket.send(packet);
			System.out.println("Done.");

			socket.close();
		}
		catch (UnknownHostException addressErr)
		{
			// Could bot create InetAddress
			addressErr.printStackTrace();
		}
		catch (SocketException socketErr)
		{
			// Could not create DatagramSocket
			socketErr.printStackTrace();
		}
		catch (IOException ioErr)
		{
			// Could not send packet
			ioErr.printStackTrace();
		}

    }
}