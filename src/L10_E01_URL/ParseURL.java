package L10_E01_URL;

/**
 * @author Robert Jonsson (robert.jonsson@miun.se), ITM Ístersund
 * @author JariH (jarih@setur.fo).
 * @version 1.1
 * @file L10_E01_ParseURL - ParseURL.java
 */

import java.net.*;

public class ParseURL
{

	public static void main(String[] args)
	{
		try
		{
			URL java = new URL("https://www.java.com/en/index.jsp");

			System.out.println(java.toExternalForm());
			System.out.println("Protocol.... " + java.getProtocol());
			System.out.println("Host........ " + java.getHost());
			System.out.println("File........ " + java.getFile());
			System.out.println("Port........ " + java.getPort());
			System.out.println("Default port " + java.getDefaultPort());
		}
		catch (MalformedURLException err)
		{
			// Error creating URL
			err.printStackTrace();
		}
    }
}