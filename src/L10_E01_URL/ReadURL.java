package L10_E01_URL;

/**
 * @author Robert Jonsson (robert.jonsson@miun.se), ITM Ístersund
 * @author JariH (jarih@setur.fo).
 * @version 1.1
 * @file L10_E01_ReadURL - ReadURL.java
 */

import java.net.*;
import java.io.*;

public class ReadURL
{

	public static void main(String[] args)
	{
		try
		{
			// Create URL
			URL java = new URL("https://www.java.com/en/");

			// Establishes an InputStream.
			InputStream is = java.openStream();

			// Read data from Stream and write to screen 
			int c;
			while ((c = is.read()) != -1)
			{
				System.out.print((char)c);
			}

			// Close Stream
			is.close();

			// We may even couple a BufferedReader onto the Stream and and read per line with readLine() 
			/*BufferedReader in = new BufferedReader(new InputStreamReader(java.openStream()));
			String s;

			while ((s = in.readLine()) != null)
			{
				System.out.println(s);
			}

			in.close();*/

		}
		catch (MalformedURLException err)
		{
			// Error creating URL
			err.printStackTrace();
		}
		catch (IOException io)
		{
			// error reading URL
			io.printStackTrace();
		}

    }
}