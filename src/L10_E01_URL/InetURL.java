package L10_E01_URL;

/**
 * @author JariH (jarih@setur.fo).
 * @version 1.1
 * @file L10_E01_URL - InetURL.java
 */

import java.net.*;
import java.util.Date;
import java.io.*;

public class InetURL
{

	public static void main(String[] args)
	{
		try
		{
			InetAddress ip1 = InetAddress.getByName("www.java.com");
			InetAddress ip2 = InetAddress.getByName("46.227.118.162");
			InetAddress ip3 = InetAddress.getByName("www.setur.fo");
			InetAddress ip4 = InetAddress.getLocalHost();


			System.out.println("IP 1....... " + ip1.toString());
			System.out.println("IP 2....... " + ip2.toString());
			System.out.println("IP 3....... " + ip3.toString());
			System.out.println("IP 4....... " + ip4.toString());
		}
		catch (IOException ioe) {
			System.err.println(ioe);
		}
    }
}