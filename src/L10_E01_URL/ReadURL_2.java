package L10_E01_URL;

import java.net.*;
import java.io.*;

public class ReadURL_2 {

	public static void main(String[] args)
	{
		try
		{
			URL java = new URL("https://www.java.com/en/");
			InputStream is = java.openStream();
			int c;
			while ((c = is.read()) != -1)
			{
				System.out.print((char)c);
				try {Thread.sleep(10);} catch (Exception e) {}
			}
			is.close();

			// We may even couple a BufferedReader onto the Stream and and read per line with readLine() 
			/*BufferedReader in = new BufferedReader(new InputStreamReader(java.openStream()));
			String s;

			while ((s = in.readLine()) != null)
			{
				System.out.println(s);
			}

			in.close();*/

		}
		catch (MalformedURLException err)
		{
			// Error creating URL
			err.printStackTrace();
		}
		catch (IOException io)
		{
			// error reading URL
			io.printStackTrace();
		}

    }
}