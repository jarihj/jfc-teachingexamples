package L10_E01_URL;

/**
 * @author JariH (jarih@setur.fo).
 * @version 1.1
 * @file L10_E01_URL - ConnectionURL.java
 */

import java.net.*;
import java.util.Date;
import java.io.*;

public class ConnectionURL
{

	public static void main(String[] args)
	{
		try
		{
			// Create URL
			URL java = new URL("https://www.java.com/en/");
			URLConnection uc = java.openConnection();

			System.out.println(java.toExternalForm());
			System.out.println("Encoding....... " + uc.getContentEncoding());
			System.out.println("ContentType.... " + uc.getContentType());
			System.out.println("ContentLength.. " + uc.getContentLength());
			System.out.println("Date........... " + new Date(uc.getDate()));
			System.out.println("Last Modified.. " + new Date(uc.getLastModified()));
		}
		catch (MalformedURLException err)
		{
			// Error creating URL
			err.printStackTrace();
		}
		catch (IOException ioe) {
			System.err.println(ioe);
		}
    }
}