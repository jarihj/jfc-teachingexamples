package L08_E06_Scaling;
//Modified by Jari � Hj�llum from http://zetcode.com/gfx/java2d/transformations/

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.geom.AffineTransform;
import javax.swing.JFrame;
import javax.swing.JPanel;

class SurfaceMod extends JPanel {

    private void doDrawing(Graphics g) {
    	GraphicsConfiguration gc = getGraphicsConfiguration();
    	System.out.println("DefaultTransform::");
    	System.out.printf("Scales %f  %f\n", gc.getDefaultTransform().getScaleX(),gc.getDefaultTransform().getScaleX());
    	System.out.println(gc.getDefaultTransform());
    	System.out.println("Device::");
    	System.out.println(gc.getDevice());

    	
        Graphics2D g2d = (Graphics2D) g.create();
        

        g2d.setColor(new Color(150, 150, 150));
        g2d.fillRect(20, 20, 80, 50);
        System.out.println(g2d+"");
        

        AffineTransform tx1;
//        tx1 = new AffineTransform();							//Toggle these two lines, and observe the difference.
        tx1=getGraphicsConfiguration().getDefaultTransform();   //Toggle these two lines, and observe the difference.
//        tx1.translate(80, 0);
        tx1.translate(80+10, 0+10);
        tx1.scale(0.5, 0.5);
        System.out.println(""+tx1);
        g2d.setTransform(tx1);
        g2d.setColor(new Color(200, 200, 0));
        g2d.fillRect(20, 20, 80, 50);
        
//        tx1 = new AffineTransform();							//Toggle these two lines, and observe the difference.
        tx1=getGraphicsConfiguration().getDefaultTransform();   //Toggle these two lines, and observe the difference.
        tx1.translate(80, 40);
        tx1.scale(0.5, 0.5);
        g2d.setTransform(tx1);
        g2d.setColor(new Color(0, 150, 150));
        g2d.fillRect(20, 20, 80, 50);

        AffineTransform tx2;
        tx2 = new AffineTransform();							//Toggle these two lines, and observe the difference.
//        tx2 = getGraphicsConfiguration().getDefaultTransform(); //Toggle these two lines, and observe the difference.
        tx2.translate(170, 20);
        tx2.scale(1.5, 1.5);

        g2d.setTransform(tx2);
        g2d.setColor(new Color(200, 0, 0));
        g2d.fillRect(20, 20, 80, 50);
        
        g2d.dispose();
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }
}

public class ScalingExMod extends JFrame {

    public ScalingExMod() {

        initUI();
    }

    private void initUI() {

        add(new SurfaceMod());

        setTitle("ScalingMod");
        setSize(330, 160);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                ScalingExMod ex = new ScalingExMod();
                ex.setVisible(true);
            }
        });
    }
}