package L03_E1_CallByValue;


class MyInt {
	int x;
}

public class CallByValueExample {
	
	public static void inc(int x) {
		System.out.println("Inside inc. Value of x before change: "+x);
		x++;
		System.out.println("Inside inc. Value of x after change: "+x);
	}
	
	public static void change(MyInt j) {
		System.out.println("Inside inc. Value of j.x before change: "+j.x);
		j = new MyInt();
		j.x=17;
		System.out.println("Inside inc. Value of j.x after change:  "+j.x);
	}
	
	public static void incValue(MyInt k) {
		System.out.println("Inside inc. Value of k.x before change: "+k.x);
		k.x++;
		System.out.println("Inside inc. Value of k.x after change:  "+k.x);
	}
	
	public static void swap(MyInt a, MyInt b) {
		System.out.printf("Inside swap. Before. (a,b)=(%d,%d)\n",a.x,b.x);
		MyInt tmp = a;
		a=b;
		b=tmp;
		System.out.printf("Inside swap. After. (a,b)=(%d,%d)\n",a.x,b.x);		
	}
	
	public static void main(String args[]) {
		System.out.println("I am here...");
		int a = 1;
		System.out.println("Outside. Value of a before change: "+a);
		inc(a);
		System.out.println("Outside. Value of a after change:  "+a);

		System.out.println("======");
		MyInt i = new MyInt();
		i.x=1;
		System.out.println("Outside. Value of i.x before change: "+i.x);
		change(i);
		System.out.println("Outside. Value of i.x after change:  "+i.x);

		System.out.println("======");
		i.x=1;
		System.out.println("Outside. Value of i.x before change: "+i.x);
		incValue(i);
		System.out.println("Outside. Value of i.x after change:  "+i.x);
		
		System.out.println("======");
		i.x=1;
		MyInt j = new MyInt();
		j.x=5;
		System.out.printf("Outside. Before swap (i,j)=(%d,%d)\n",i.x,j.x);
		swap(i,j);
		System.out.printf("Outside. After swap  (i,j)=(%d,%d)\n",i.x,j.x);
		System.out.println(i.toString());
		System.out.println(j.toString());
		
	}

}
