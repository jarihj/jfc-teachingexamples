package L06_E06;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadPrimitives {

	public static void main(String[] args) {
		try {
			DataInputStream in = new DataInputStream(new FileInputStream("data.dat"));
			String a = in.readUTF();
			String b = in.readUTF();
			int   no = in.readInt();
			String c = in.readUTF();
			System.out.println("READ::\n"+a+b+no+c);
			in.close();
		} //try
		catch (FileNotFoundException e) {System.err.println("FNFE ::"+e);}
		catch (IOException e) {System.err.println("IOE ::"+e);}
	}

}
