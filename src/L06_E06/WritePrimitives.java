package L06_E06;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class WritePrimitives {

	public static void main(String[] args) {
		try {
			DataOutputStream out = new DataOutputStream(new FileOutputStream("data.dat"));
			out.writeUTF("First string. ������ etc....\n");
			out.writeUTF("Are all UTF characters. Next line will contain an int:\n");
			out.writeInt(42);
			out.writeUTF("===END OF FILE===");
			out.close();
		} //try
		catch (FileNotFoundException e) {System.err.println("FNFE ::"+e);}
		catch (IOException e) {System.err.println("IOE ::"+e);}
	}

}
