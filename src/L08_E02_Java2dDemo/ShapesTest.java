package L08_E02_Java2dDemo;

import java.awt.*;
import java.awt.desktop.ScreenSleepEvent;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.awt.event.MouseAdapter;

import javax.swing.*;


public class ShapesTest extends JFrame {
	class ClickEvent extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			JPaintPanel p = (JPaintPanel) e.getSource();
			ShapesTest f = (ShapesTest) SwingUtilities.getWindowAncestor(p);
			f.paintShapes(e);
			System.out.println(e+"");
		} 
	}

	
	JPaintPanel pnl;
	public ShapesTest() {
		setSize(new Dimension(500,400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pnl = new JPaintPanel();
		add(pnl,BorderLayout.CENTER);
		pnl.addMouseListener(new ClickEvent());
		setVisible(true);
	}
	
	public void paintShapes(MouseEvent me) {
		Graphics g=pnl.getGraphics();
		Random random = new Random();
		for (int i=0;i<1;i++) {
			g.setColor(new Color(random.nextInt(256*256*256)));
			int x1 = random.nextInt(getWidth());
			int x2 = random.nextInt(getWidth());
			int y1 = random.nextInt(getHeight());
			int y2 = random.nextInt(getHeight());
			x1=me.getX();
			y1=me.getY();
			int q = random.nextInt(2);
			switch (random.nextInt(4)) {
			case 0: {
				if (q==0) g.drawRect(x1, y1, Math.abs(x2-x1), Math.abs(y2-y1));
				else g.fillRect(x1, y1, Math.abs(x2-x1), Math.abs(y2-y1));
				break;
			}
			case 1: {
				int rx = Math.abs(x2-x1)/2;
				int ry = Math.abs(y2-y1)/2;
				if (q==0) g.drawOval(x1-rx, y1-ry,2*rx, 2*ry);
				else g.fillOval(x1-rx, y1-ry,2*rx, 2*ry);
				break;
			}
			case 2: {
				int rx = Math.abs(x2-x1)/2;
				int ry = Math.abs(y2-y1)/2;
				if (q==1) g.drawArc(x1-rx, y1-ry,2*rx, 2*ry, random.nextInt(360), random.nextInt(360));
				else g.fillArc(x1-rx, y1-ry,2*rx, 2*ry, random.nextInt(360), random.nextInt(360));
				break;
			}
			case 3: {
				int l = random.nextInt(10)+2;
				int [] ii = new int[l];
				int [] jj = new int[l];
				ii[0]=me.getX();
				jj[0]=me.getY();
				
				for (int j=1; j<l;j++) {
					ii[j]=random.nextInt(getWidth());
					jj[j]=random.nextInt(getHeight());
				}
				if (q==1) g.drawPolygon(new Polygon( ii,jj,l));
				else g.fillPolygon(new Polygon( ii,jj,l));
				break; 
			}

			default:
				
			}
//			try {Thread.sleep(100);} catch (Exception e) {}
		}

	}
	
   
    public static void main(String[] args) {
    	ShapesTest st = new ShapesTest(); 
    }
}