package L11_E04_SortingExample;

import java.util.Comparator;

public class DescendingAlphabeticalComparator implements Comparator<String> {
	@Override
	public int compare(String a, String b) {
		return b.compareTo(a);
	}
}