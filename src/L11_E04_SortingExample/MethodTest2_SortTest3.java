package L11_E04_SortingExample;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MethodTest2_SortTest3 {
	public static void main(String[] args) {
		List<String> names = Arrays.asList("C++", "Java", "JSP", "Python","Pascal",	"C#", "Cobol");
		Comparator<String> cmp = String::compareTo;
		Collections.sort(names, cmp); // New
//		Collections.sort(names, (a,b) -> a.compareTo(b)); //Old
		System.out.println(names.toString());
	}

}
