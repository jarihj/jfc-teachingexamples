package L11_E04_SortingExample;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortTest2 {
	public static void main(String[] args) {
		List<String> names = Arrays.asList("C++", "Java", "JSP", "Python","Pascal",	"C#", "Cobol");
		Collections.sort(names, new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				return b.compareTo(a);
			}
		});
		System.out.println(names.toString());
	}

}
