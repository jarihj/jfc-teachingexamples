package L11_E04_SortingExample;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortTest1 {

	public static void main(String[] args) {
		List<String> names = Arrays.asList("C++", "Java", "JSP", "Python","Pascal",	"C#", "Cobol");
		Collections.sort(names, new DescendingAlphabeticalComparator());
		System.out.println(names.toString());
	}

}
