package L09_E05_TestPriority_NotWorking;

public class TestingPriority {
	
	public static void main(String[] args) {
		System.out.println("Start");
		SleeplessAnimal cow = new SleeplessAnimal("Cow","muuu");
		SleeplessAnimal dog = new SleeplessAnimal("Dog","vuff");
		SleeplessAnimal cat = new SleeplessAnimal("Cat","mjav");
		cow.setPriority(Thread.MIN_PRIORITY);
		cat.setPriority(Thread.MAX_PRIORITY);
		cow.talk();
		dog.talk();
		cat.talk();
		try {
			cow.join();
			dog.join();
			cat.join();
		}
		catch (InterruptedException ie) {}
		System.out.println("End");
	}
}
