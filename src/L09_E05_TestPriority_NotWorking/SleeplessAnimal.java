package L09_E05_TestPriority_NotWorking;

public class SleeplessAnimal extends Thread {
	private String type, sound;
	public SleeplessAnimal(String type, String sound) {
		this.type = type; 
		this.sound = sound;
	}
	
	public void talk() {
		start();
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println(sound + " " + (i+1) + " from " + type + " ("+getPriority()+")");
		}
	}
}