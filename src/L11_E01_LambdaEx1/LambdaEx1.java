package L11_E01_LambdaEx1;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.*;

import javax.swing.*;

public class LambdaEx1 extends JFrame implements ActionListener{
	JButton jb1, jb2, jb3;
	public LambdaEx1() {
		setLayout(new FlowLayout());
		setTitle(this.getClass().getName());
		setSize(400, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Add JButton 1
		jb1 = new JButton("Test1");
		jb1.addActionListener(this);
		add(jb1);
		
		// Add JButton 2
		jb2 = new JButton("Test2");
		jb2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Hello World 2");
			}
		});
		add(jb2);
		
		// Add JButton 3
		jb3 = new JButton("Test3");
		jb3.addActionListener(e -> System.out.println("Hello world 3"));
		add(jb3);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==jb1) {
			System.out.println("Hello World 1");
		}
	}
	
	public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
        		new LambdaEx1();
            }
        });
	}

}
