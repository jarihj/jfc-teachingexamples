package L09_E01_JOptionPane;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class BigValueJOptionpaneDialog {

  public static void main(String[] a) {
    JFrame frame = new JFrame();
    String bigList[] = new String[20];
//    Object bigList[] = {"A","B","C"};
//    String bigList[] = {"A","B","C"};

    for (int i = 0; i < bigList.length; i++) {
      bigList[i] = Integer.toString(i);
    }
    // If 20 options are available JOptionPane changes to using List in stead of using dropdownboxes
    JOptionPane.showInputDialog(frame, "Pick a printer", "Input", JOptionPane.QUESTION_MESSAGE, null, bigList, "1");

  }

}