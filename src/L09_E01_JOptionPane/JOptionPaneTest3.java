package L09_E01_JOptionPane;
// From http://www.java2s.com/Tutorial/Java/0240__Swing/UsingJOptionPanewithapredefinedselections.htm
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.*;
import javax.swing.UnsupportedLookAndFeelException;

public class JOptionPaneTest3 extends JFrame{
  public static void main(String[] args) {
    JDialog.setDefaultLookAndFeelDecorated(true);    
    Object[] selectionValues = { "Pandas", "Dogs", "Horses" };
    String initialSelection = "Dogs";
    Object selection = JOptionPane.showInputDialog(null, "What are your favorite animals?",
        "Zoo Quiz", JOptionPane.QUESTION_MESSAGE, null, selectionValues, initialSelection);
    System.out.println(selection);
  }
}