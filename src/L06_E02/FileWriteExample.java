package L06_E02;
import java.io.*;

public class FileWriteExample {
	public static void main(String args[]) {
		try {
			FileWriter fw = new FileWriter("filename.txt");
			BufferedWriter fileOut = new BufferedWriter(fw);
			fileOut.write("Programming in ");
			fileOut.newLine();
			fileOut.close();
		}
		catch (IOException ioerr) {
			System.err.println("error"+ioerr);
		}
	}

}
