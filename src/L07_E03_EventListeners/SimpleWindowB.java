 package L07_E03_EventListeners;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;

class ActionEvent_JH implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		JOptionPane.showMessageDialog(null,btn.getText());
	}
}

public class SimpleWindowB extends JFrame  {
	JButton btnA, btnB, btnC;
	public SimpleWindowB() {
		setLayout(new FlowLayout());
		btnA=new JButton("I am A");
		btnA.setSize(new Dimension(50,20));
		add(btnA);
		btnA.addActionListener(new ActionEvent_JH());

		btnB=new JButton("I am B");
		btnB.setSize(new Dimension(50,20));
		add(btnB);
		btnB.addActionListener(new ActionEvent_JH());

		btnC=new JButton("I am C");
		btnC.setSize(new Dimension(50,20));
		add(btnC);
		btnC.addActionListener(new ActionEvent_JH());

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}
	
	public static void main(String[] args) {
		SimpleWindowB sw = new SimpleWindowB();
	}

}
