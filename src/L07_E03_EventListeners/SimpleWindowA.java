 package L07_E03_EventListeners;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;

import javax.swing.*;

public class SimpleWindowA extends JFrame implements ActionListener {
	JButton btnA, btnB, btnC;
	public SimpleWindowA() {
		setLayout(new FlowLayout());
		btnA=new JButton("I am A");
		btnA.setSize(new Dimension(50,20));
		add(btnA);
		btnA.addActionListener(this);

		btnB=new JButton("I am B");
		btnB.setSize(new Dimension(50,20));
		add(btnB);
		btnB.addActionListener(this);

		btnC=new JButton("I am C");
		btnC.setSize(new Dimension(50,20));
		add(btnC);
		btnC.addActionListener(this);

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==btnA) {JOptionPane.showMessageDialog(null,"A");}
		else if (e.getSource()==btnB) {JOptionPane.showMessageDialog(null,"B");}
		else if (e.getSource()==btnC) {JOptionPane.showMessageDialog(null,"C");}
	}
	
	public static void main(String[] args) {
		SimpleWindowA sw = new SimpleWindowA();
	}

}
