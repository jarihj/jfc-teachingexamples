 package L07_E03_EventListeners;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;

import javax.swing.*;

public class VerySimple extends JFrame implements ActionListener {
	JButton btnA; 
	public VerySimple() {
		setLayout(new FlowLayout());
		btnA=new JButton("I am A");
		btnA.setSize(new Dimension(50,20));
		add(btnA);
		btnA.addActionListener(this);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(null,"JButton pressed");
	}
	
	public static void main(String[] args) {
		VerySimple sw = new VerySimple();
	}

}
