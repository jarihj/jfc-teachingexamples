 package L07_E03_EventListeners;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;


public class SimpleWindowC extends JFrame  {
	JButton btnA, btnB, btnC;
	public SimpleWindowC() {
		setLayout(new FlowLayout());
		btnA=new JButton("I am A");
		btnA.setSize(new Dimension(50,20));
		add(btnA);
		btnA.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JButton btn = (JButton) e.getSource();
					JOptionPane.showMessageDialog(null,btn.getText());
				}
		});

		btnB=new JButton("I am B");
		btnB.setSize(new Dimension(50,20));
		add(btnB);
		btnB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton btn = (JButton) e.getSource();
				JOptionPane.showMessageDialog(null,btn.getText());
			}
	});

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the window when "X" is pressed.
		setSize(300, 200); // Window Size
		setVisible(true); // Show the window
	}

	public static void main(String[] args) {
		SimpleWindowC sw = new SimpleWindowC();
	}

}
