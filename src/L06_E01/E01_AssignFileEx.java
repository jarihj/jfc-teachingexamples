package L06_E01;

import java.io.File;

public class E01_AssignFileEx {
	
	public static void mkdir() {
		File folder = new File("test-folder");
		folder.mkdir();
		if (folder.exists()) System.out.printf("Folder %s does exist.\n",folder.getName()); 
		else System.out.printf("Folder %s does NOT exist.\n",folder.getName());
	}
	
	public static void ren() {
		File file = new File("test.txt");
		File toFile = new File("test2.txt");
		file.renameTo(toFile);
		if (toFile.exists()) System.out.printf("File %s does exist.\n",toFile.getName()); 
		else System.out.printf("File %s does NOT exist.\n",toFile.getName());

	}

	public static void main(String[] args) {
		File file = new File("test.txt");
		if (file.exists()) System.out.printf("File %s does exist.\n",file.getName()); 
		else System.out.printf("File %s does NOT exist.\n",file.getName());
		
		File folder = new File("test-folder");
		if (folder.exists()) System.out.printf("Folder %s does exist.\n",folder.getName()); 
		else System.out.printf("Folder %s does NOT exist.\n",folder.getName());
		
		File f2 = new File("C:\\Users\\jarih\\OneDrive - Fr��skaparsetur F�roya\\Forritan\\Java\\JavaCourse-TeachingExamples\\test.txt");
		
		ren();
		mkdir();
	}

}
