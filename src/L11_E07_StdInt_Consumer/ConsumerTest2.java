package L11_E07_StdInt_Consumer;

import java.util.function.Consumer;

public class ConsumerTest2 {
	public static void main(String[] args) {
		User user = new User("Jari", "admin");
		Consumer<User> printName = u -> System.out.println("Name: " +u.getName());
		Consumer<User> printRole = u -> System.out.println("Role: " +u.getRole());
		printName.andThen(printRole).accept(user); 
	}

}
