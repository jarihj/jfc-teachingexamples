package L11_E07_StdInt_Consumer;

public class User {
	private String name;
	private String role;
	public User(String name, String role) {
		this.name = name;
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRole() {
		return role;
	}
}