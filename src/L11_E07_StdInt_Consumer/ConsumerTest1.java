package L11_E07_StdInt_Consumer;

import java.util.function.Consumer;

public class ConsumerTest1 {
	public static void main(String[] args) {
		User user = new User("Jari", "admin");
		Consumer<User> printName = u -> System.out.println("Name: " +u.getName());
		printName.accept(user); // Name: Robert
	}

}
