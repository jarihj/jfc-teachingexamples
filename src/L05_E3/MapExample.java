package L05_E3;

import java.util.*;

public class MapExample {
	Map map;
	
	public void printAll() {
	    Set set=map.entrySet();//Converting to Set so that we can traverse  
	    Iterator itr=set.iterator();
	    while(itr.hasNext()){  
	        //Converting to Map.Entry so that we can get key and value separately  
	        Map.Entry entry=(Map.Entry)itr.next();  
	        System.out.println(entry.getKey()+" "+entry.getValue());  
	    }
	}
	
	public void run() {
	    map=new HashMap();  
	    //Adding elements to map  
	    map.put(1,"Anders");  
	    map.put(5,"Joakim");  
	    map.put(2,"Rip");  
	    map.put(6,"Fedtmule");  
	    //Traversing Map  
	    System.out.println("==First print::");
	    printAll();
	    
	    System.out.println("==Removed an element::");
	    if (map.remove(5)!=null) System.out.println("Removed successfully");
	    else System.out.println("Remove failed");
	    printAll();

	    System.out.println("==Removed an element::");
	    if (map.remove(5)!=null) System.out.println("Removed successfully");
	    else System.out.println("Remove failed");
	    printAll();

	    System.out.println("==Added an element::");
	    map.put(7,"Mickey");
	    printAll();
	}
	
	public static void main(String[] args) {  
		MapExample me = new MapExample();
		me.run();
	    
	}  
}  