package L08_E03_GraphicsConfiguration;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.*;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.*;
class JTextAreaX extends JTextArea {
	public void appendLine(String str) {
		super.append(str+"\n");
	}
}

public class TestGraphicsConfiguration extends JFrame {
	JTextAreaX txt ;
	public TestGraphicsConfiguration() {
		txt = new JTextAreaX();
		txt.setFont(new Font("Courier", Font.PLAIN,14));
		add(txt,BorderLayout.CENTER);
		JScrollPane scrollPane = new JScrollPane(txt); 
		txt.setEditable(false);
		add(scrollPane,BorderLayout.CENTER);
		
		GraphicsConfiguration gc = getGraphicsConfiguration();
		txt.appendLine("====GraphicsConfiguration::====");
		txt.appendLine(""+gc);
		printGraphicsConfiguration(gc, "  ");
		
		GraphicsDevice gd = gc.getDevice();
		txt.appendLine("====GraphicsDevice::=====");
		txt.appendLine(""+gd);
		printGraphicsDevice(gd, "  ");
		
		txt.appendLine("====GraphicsEnvironment::=====");
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		txt.appendLine(""+ge);
		txt.appendLine("  +-MaxiumWindowBounds:"+ge.getMaximumWindowBounds());
	    
	    GraphicsDevice[] devices = ge.getScreenDevices();
	    for (int i=0;i<devices.length;i++) {
	    	txt.appendLine(String.format("  Device: %d (%d)",(i+1),devices.length));
	    	printGraphicsDevice(devices[i], "    ");
		    txt.appendLine("");
	    }
	    GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
	    txt.appendLine("Default screen device: "+ defaultScreen.getIDstring());
	    printGraphicsDevice(defaultScreen,"  ");
//	    txt.appendLine(String.format("Resolution: %d x %d ",defaultScreen.getFullScreenWindow().getSize().width,defaultScreen.getFullScreenWindow().getSize().height));

	    
	    
	    setSize(new Dimension(500,400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}
	
	public void printGraphicsDevice(GraphicsDevice device, String pre) {
    	txt.appendLine(pre+"+-IDstring:   "+device.getIDstring());
    	txt.appendLine(pre+"+-AvailableAcceleratedMemory: "+device.getAvailableAcceleratedMemory());
    	txt.appendLine(pre+"+-Type: "+device.getType());
    	
    	DisplayMode[] dm =  device.getDisplayModes();
    	txt.appendLine(pre+"+-DisplayModes: "+dm.length);
    	txt.append(pre+"  +-Modes: ");
	    for (int j=0;j<dm.length;j++) { txt.append(dm[j].toString()); }txt.appendLine("");
	    
	    GraphicsConfiguration[] configurations = device.getConfigurations();
	    txt.appendLine(pre+"+-Configurations summary: "+configurations.toString());
	    txt.appendLine(pre+"+-Configurations count : "+configurations.length);
	    txt.append(pre+"  +-Configurations: ");
	    for (int j=0;j<configurations.length;j++) { txt.append(configurations[j].toString()); }txt.appendLine("");
	    for (int j = 0; j < configurations.length; j++) {
		      txt.appendLine(String.format(pre+"+-Configuration %d (%d)",(j + 1),configurations.length));
		      printGraphicsConfiguration(configurations[j],pre+"  ");
		    }	    
	}
	
	public void printGraphicsConfiguration(GraphicsConfiguration configuration, String pre) {
	      txt.appendLine(pre+"+-" + configuration.getColorModel());
	      txt.appendLine(pre+"+-" + configuration.getBounds());
	      txt.appendLine(String.format(pre+"+-Bounds: %f x %f",configuration.getBounds().getWidth(),configuration.getBounds().getHeight()));
	      txt.appendLine(pre+"+-DefaultTransform:" + configuration.getDefaultTransform());
	      txt.appendLine(pre+"+-NormalizingTransform:" + configuration.getNormalizingTransform());
	      txt.appendLine(pre+"+-ImageCapabilities:" + configuration.getImageCapabilities());
	      txt.appendLine(pre+"  +-Accelerated:" +configuration.getImageCapabilities().isAccelerated());
	      txt.appendLine(pre+"  +-TrueVolatile:" +configuration.getImageCapabilities().isTrueVolatile());
	}
	
	public static void main(String[] args) {
		TestGraphicsConfiguration t = new TestGraphicsConfiguration();
	}

}

