package L05_E1;

import java.util.Iterator;
import java.util.Vector;

public class IteratorExample {
	
	public static void main(String args[]) {
		Vector<Integer> vec = new Vector<Integer>();
		for (int i = 0; i<10; i++) 
				vec.add(i*i);
		Iterator<Integer> it = vec.iterator();
		
		while (it.hasNext()) {
			Integer value = it.next();
			System.out.println(""+value);
		}
	}

}
