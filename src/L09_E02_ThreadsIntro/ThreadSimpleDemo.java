package L09_E02_ThreadsIntro;

public class ThreadSimpleDemo {

	public static void main(String[] args) {
		MyThread mt = new MyThread();
		mt.start();
		(new MyThread(10)).start();
	}

}
