package L09_E02_ThreadsIntro;

public class MyThread extends Thread {
	int iStart;
	public MyThread() {
		this(0);
	}
	
	public MyThread(int iStart) {
		this.iStart=iStart;
	}
	public void run()  {
		System.out.println("Starting thread");
		for (int i=iStart; i<iStart+7; i++) { 
			System.out.printf("The number is: %d. The square is %d and the squareroot is %f\n",
								i,i*i,Math.sqrt(i)); 
			try {sleep (1000);} catch (InterruptedException ie) {}
		}
		System.out.println("Thread ending");
	}
}