package L11_E02_FunctionalInterfacesEx;



public class FuncInterfaceEx {
	
	public void run() {
		Formula addFormula = new Formula() {
			public double calculate(double a, double b) {
				return a+b;
			}
		};
		double a = 3;
		double b = 4;
		System.out.printf("The sum of %.1f and %.1f is %.1f.\n",
				a,b,addFormula.calculate(a,b));
	}
	
	public static void main(String[] args) {
		new FuncInterfaceEx().run();
	}

}
