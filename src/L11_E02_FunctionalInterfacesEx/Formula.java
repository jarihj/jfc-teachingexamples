package L11_E02_FunctionalInterfacesEx;

@FunctionalInterface
public interface Formula {
	public double calculate(double a, double b);
}