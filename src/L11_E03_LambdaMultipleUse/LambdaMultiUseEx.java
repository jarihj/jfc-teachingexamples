package L11_E03_LambdaMultipleUse;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;

import L11_E01_LambdaEx1.LambdaEx1;

public class LambdaMultiUseEx extends JFrame{
	
	JButton jb1, jb2, jb3, jb4,jb5;
	public LambdaMultiUseEx() {
		setLayout(new FlowLayout());
		setTitle(this.getClass().getName());
		setSize(400, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ActionListener l = e -> System.out.println("Hello World!");
		jb1 = new JButton("use");
		jb1.addActionListener(l);
		add(jb1);
		
		jb2 = new JButton("reuse");
		jb2.addActionListener(l);
		add(jb2);
		
		jb3 = new JButton("LambdaStrExpr");
		jb3.addActionListener(e -> System.out.println("T�smorgun "+getLambdaStr().expr()));
		add(jb3);

		
		jb4 = new JButton("LambdaActionExpr");
		jb4.addActionListener(getLambdaActionExpr());
		add(jb4);
		
		jb5 = new JButton("LambdaActionExpr2");
		jb5.addActionListener(getLambdaActionExpr2());
		add(jb5);
	}
	
	public LambdaStrExpr getLambdaStr() {
		return () -> "Hello World from returned LambdaStrExpr";
	}
	
	public ActionListener getLambdaActionExpr() {
		return (ActionEvent e) -> System.out.println(getLambdaStr().expr()+
				" from returned LambdaActionExpr");
	}
	
	public LambdaActionExpr getLambdaActionExpr2() {
		return (ActionEvent e) -> System.out.println(getLambdaStr().expr()+
				" from returned LambdaActionExpr FunctionalInterface");
	}
	
	public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
        		new LambdaMultiUseEx();
            }
        });
	}

}
