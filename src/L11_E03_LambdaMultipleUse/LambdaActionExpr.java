package L11_E03_LambdaMultipleUse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@FunctionalInterface
public interface LambdaActionExpr extends ActionListener{
//	public void actionPerformed(ActionEvent e);
}
