package L11_E03_LambdaMultipleUse;

@FunctionalInterface
public interface LambdaStrExpr {
	public String expr();
}
