package L05_E4;

public class StudentTest {

	public void compareAndPrint(Student stud1, Student stud2)
	{
		if(stud1.compareTo(stud2)==0)
			System.out.println(stud1 + " equals " + stud2);
		else if(stud1.compareTo(stud2)<0)
			System.out.println(stud1 + " is less than " + stud2);
		else
			System.out.println(stud1 + " is larger than " + stud2);
	}
	
	public static void main (String args[]) {
		Student s1 = new Student("Mickey Mouse","190413-1213");
		Student s2 = new Student("Donald Duck","100313-1217");
		Student s3 = new Student("Clark Kent","170202-1243");
		Student s4 = new Student("King Leonidas","190413-1213");
		StudentTest test = new StudentTest();
		test.compareAndPrint(s1,s2);
		test.compareAndPrint(s1,s3);
		test.compareAndPrint(s1,s4);
		test.compareAndPrint(s2,s3);
		test.compareAndPrint(s2,s4);
		test.compareAndPrint(s3,s4);
	}
}