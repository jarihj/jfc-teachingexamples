package L05_E4;

public class Student implements Comparable<Student> {
	private String iName;
	private String iPnr;
	public Student(String aName, String aPnr) {
		this.iName=aName;
		this.iPnr=aPnr;
	}
	public String getPnr(){return iPnr;}
	
	public String toString() {
		return iName+", "+iPnr;
	}

	public boolean equals(Object o) {
		Student s=(Student)o;
		boolean b=iPnr.equals(s.getPnr());
		return (b);
	}
	public int compareTo(Student s) {
		int i=iPnr.compareTo(s.getPnr());
		return (i);
	}
}